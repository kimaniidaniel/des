import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, Events } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DataserviceProvider } from '../../providers/dataservice/dataservice';
import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  appInfo = `${this.dataService.settings.app_name} v${this.dataService.settings.app_version}`;
  my_sessions = this.dataService.data.my_sessions;

  constructor(
    public navCtrl: NavController,
    private barcodeScanner: BarcodeScanner,
    private browser: InAppBrowser,
    private toastCtrl: ToastController,
    private dataService: DataserviceProvider,
    private alertCtrl: AlertController,
    private events: Events
  ) {
    this.events.subscribe('data:my_sessions', x=> {
      this.my_sessions = this.dataService.data.my_sessions;
    })
  }

  ionViewDidLoad(){
    console.log('my_sessions',this.my_sessions);
  }

  scan() {
    this.barcodeScanner.scan(
      {
        showFlipCameraButton: true, // iOS and Android
        showTorchButton: true, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        prompt: "Place a barcode inside the scan area", // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        //orientation: "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
        //disableAnimations: false, // iOS
        //disableSuccessBeep: false // iOS and Android
      }).then(
        (result) => {
          let msg = "We got a barcode\n" + "Result: " + result.text + "\n" +
            "Format: " + result.format + "\n" +
            "Cancelled: " + result.cancelled;
          this.presentSigninPrompt(result.text);
          console.log(result, msg);
        },
        (error) => {
          this.showToastMessage("Scanning failed: " + error);
          console.error(error);
        },
    )
  }

  isValidEmail(email: string) {
    return email && email.endsWith('@sgu.edu');
  }

  isValidStudentId(student_id: string) {
    return student_id && student_id.startsWith('A00');
  }

  isStudentRegistered(){
    return this.dataService.data.student.email;
  }

  getDay(x){
    return moment(x).format('DD');
  }

  getMonth(x){
    return moment(x).format('MMM');
  }

  getTime(x){
    return moment(x).calendar();
  }

  showToastMessage(message: string, delay: number = 4000) {
    this.toastCtrl.create({
      message: message,
      duration: delay,
      position: 'top'
    }).present();
  }

  sessionInfo(session){
    let alert = this.alertCtrl.create({
      title: 'Session Infomation',
      message: `${session.item} - ${session.location}<br>${this.getTime(session.startdatestring)}<br>${session.comment}`,
      buttons: ['Dismiss', {
        text: 'Scan Attendance',
        cssClass: 'button-default',
        handler: data => {
          this.scan();
        }
      }]
    });
    alert.present();
  }

  presentLoginPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Student Sessions',
      inputs: [
        {
          name: 'email',
          placeholder: 'SGU Email (@SGU.EDU)',
          type: 'email',
          value: this.dataService.data.student.email
        },
        {
          name: 'studentId',
          placeholder: 'Student Id (A00...)',
          value: this.dataService.data.student.id
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Login',
          handler: data => {
            if (!this.isValidStudentId(data.studentId)) {
              this.showToastMessage('This student id is invalid');
              return false;
            }
            if (!this.isValidEmail(data.email)) {
              this.showToastMessage('This is an invalid email');
              return false;
            }
            if (this.isValidEmail(data.email) && this.isValidStudentId(data.studentId)) {
              // logged in!
              this.dataService.data.student.email = data.email;
              this.dataService.data.student.id = data.studentId;
              this.dataService.initializeSignups();
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  presentSigninPrompt(session: string, title:string = 'Session sign in') {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: session,
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        },
        {
          name: 'studentId',
          placeholder: 'Student ID'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
            return;
          }
        },
        {
          text: 'Sign in',
          handler: data => {
            if (!this.isValidStudentId(data.studentId)) {
              this.showToastMessage('This student id is invalid');
              return false;
            }
            if (this.isValidEmail(data.email)) {
              this.dataService.submitSignIn(data.email, data.session);
            } else {
              this.showToastMessage('This is an invalid email');
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  //--Send students to the insight student portal
  goInsight() {
    this.dataService.launchUrl('https://sgu-insight.symplicity.com/students/');
  }

}
