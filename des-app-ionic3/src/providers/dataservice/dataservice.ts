import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Events } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/*
  Generated class for the DataserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataserviceProvider {

  public settings = {
    data_refresh_hrs: 2,
    continuous_scan: true,
    checkin_user: null,
    app_version: "0.0.1",
    app_name: 'Educational Services App',
    device_model: null,
    device_id: null,
    hot_update_version: null
  };

  public data = {
    my_sessions: [],
    student: {email: 'ahamilt1@sgu.edu', id: null} //!!IMPORTANT-- remove 'ahamilt1@...' for production
  }

  private signinsQueue: Array<{ email: string, session: string }> = [];
  public signupCollection: AngularFirestoreCollection<{}>;

  constructor(
    public http: HttpClient, 
    private storage: Storage, 
    private toastCtrl: ToastController,
    private afs: AngularFirestore,
    private events: Events,
    private browser: InAppBrowser
  ) {
    console.log('Hello DataserviceProvider Provider');
    this.initializeSignups();
  }

  setSettings(settings: any) {
    this.settings = settings;
    this.storage.set('settings', settings);
  }
  
  initializeSignups(){
    this.signupCollection = this.afs.collection('signups', ref=>{
      return ref.where('email','==',(this.data.student.email||'ahamilt1@sgu.edu')); //!!IMPORTANT-- remove 'ahamilt1@...' for production
    });
    let signups = this.signupCollection.valueChanges();
    signups.subscribe(x=>{
      console.log('Value changes',x);
      this.data.my_sessions = x;
      this.events.publish('data:my_sessions',x);
    })
    console.log(`signups`,signups);
  }

  showToastMessage(message: string, delay: number = 3000) {
    this.toastCtrl.create({
      message: message,
      duration: delay,
      position: 'top'
    }).present();
  }


  submitSignIn(email: string = null, session: string = null) {
    if (email && session) this.signinsQueue.push({ email, session });

    for (let i = 0; i < this.signinsQueue.length; i++) {
      let t = this.signinsQueue.pop();
      let url = `http://sgu.co1.qualtrics.com/jfe/form/SV_ekZpq3QIPY4lyKx/?event=${t.session}&student_email=${email}`;

      this.http.get(url).subscribe(
        res => {
          this.showToastMessage(`Successfully signed in to session ${session}`, 7000);
        },
        err => {
          console.error(err);
          this.signinsQueue.push(t);
          this.showToastMessage(`Error occured while submitting sign in, WILL AUTOMATICALLY RETRY IN 10 seconds`, 8000);
          setTimeout(() => { this.submitSignIn(); }, 10000);
        }
      )
    }
  }

  launchUrl(url: string, target: string = '_blank', options:any={ hardwareback: 'no', footer: 'yes', location: 'no' }) {
    this.browser.create(url, target, options);
  }
}
