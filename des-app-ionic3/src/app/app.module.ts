import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AppVersion } from '@ionic-native/app-version';
import { HttpClientModule  } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser'; //https://blog.paulhalliday.io/2017/07/10/ionic-3-integrating-inappbrowser-plugin/
import { Device } from '@ionic-native/device'; //https://ionicframework.com/docs/native/device/
import { CodePush, InstallMode } from '@ionic-native/code-push'; //https://ionicframework.com/docs/native/code-push/
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DataserviceProvider } from '../providers/dataservice/dataservice';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from '../firebaseconfig';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'DB_des',
         driverOrder: ['sqlite', 'indexeddb', 'websql']
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(), //used for offline storage
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    AppVersion,
    InAppBrowser,
    Device,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataserviceProvider
  ]
})
export class AppModule {}
