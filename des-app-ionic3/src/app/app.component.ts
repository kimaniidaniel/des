import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AppVersion } from '@ionic-native/app-version';
import { DataserviceProvider } from '../providers/dataservice/dataservice';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  private s: any = {app_name: 'DES', app_version: 0.1};

  constructor(platform: Platform, statusBar: StatusBar, private dataService: DataserviceProvider, splashScreen: SplashScreen, private appVersion: AppVersion) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      if (platform.is('cordova')) {
        let s = dataService.settings;
      Promise.all([this.appVersion.getAppName(), this.appVersion.getVersionNumber()])
          .then(x => {
            s.app_name = x[0];
            s.app_version = x[1];
            this.dataService.setSettings(s);
          });
        }
    });
  
  }
}

